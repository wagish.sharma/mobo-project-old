package com.kaleidofin.mobo.network;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.kaleidofin.mobo.BuildConfig;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitServiceFactory {

  private static final String MOBO_NETWORK_BASE_URL = "http://54.145.115.76:8080/";

  public static final MoboApiService MOBO_NETWORK_SERVICE;

  public static  Retrofit retrofit;

  static {
    OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
        .readTimeout(30, TimeUnit.SECONDS)
        .connectTimeout(30, TimeUnit.SECONDS)
        .addInterceptor(new FirebaseUserIdTokenInterceptor());
    if(BuildConfig.DEBUG){
      HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
      loggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);
      okHttpClientBuilder.addInterceptor(loggingInterceptor);
      okHttpClientBuilder.addNetworkInterceptor(new StethoInterceptor());
    }
    retrofit = new Retrofit.Builder()
        .baseUrl(MOBO_NETWORK_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClientBuilder.build()).build();
    MOBO_NETWORK_SERVICE = retrofit.create(MoboApiService.class);

  }
}
