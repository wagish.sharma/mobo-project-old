package com.kaleidofin.mobo.network;

import com.kaleidofin.mobo.model.dto.Goal;
import com.kaleidofin.mobo.model.dto.SpendingPatternGroup;
import io.reactivex.Single;
import java.math.BigDecimal;
import java.util.List;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MoboApiService {

  @GET("api/customer/defaultSpendingPattern")
  Single<SpendingPatternGroup> getDefaultSpendingPattern();

  @GET("api/customer/spendingPattern/overall")
  Single<SpendingPatternGroup> getOverallSpendingPattern();

  @GET("api/customer/goal/create")
  Single<Goal> createGoal(@Query("goalName") String goalName,
      @Query("goalAmount")BigDecimal goalAmount,
      @Query("tenure") Long tenure);

  @GET("api/customer/goal/")
  Single<List<Goal>> getCustomerGoals();
}
