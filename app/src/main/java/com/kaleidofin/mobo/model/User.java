package com.kaleidofin.mobo.model;

import androidx.databinding.BaseObservable;


import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class User extends BaseObservable implements Serializable {

    private String id;

    private String name;
    private String phoneNumber;
    private String avatar;
    private boolean isNew;

    public User() {
        this.id = UUID.randomUUID().toString().replace("-", "");
    }
    public User(String id) {
        this.id = id;
    }
    public User(String email, String password) {
        this(UUID.randomUUID().toString());
        this.phoneNumber = phoneNumber;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }



    public void setPhoneNumber(String email) {
        this.phoneNumber = phoneNumber;
    }


    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", isNew=" + isNew +
                '}';
    }
}
