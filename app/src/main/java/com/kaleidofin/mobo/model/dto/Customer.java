package com.kaleidofin.mobo.model.dto;


public class Customer  {

    private Long id;
    private Integer version;
    private String name;
    private String moblie;
    private String aaVui;
    private String profile;
    private String email;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Integer getVersion() {
        return version;
    }
    public void setVersion(Integer version) {
        this.version = version;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getMoblie() {
        return moblie;
    }
    public void setMoblie(String moblie) {
        this.moblie = moblie;
    }
    public String getAaVui() {
        return aaVui;
    }
    public void setAaVui(String aaVui) {
        this.aaVui = aaVui;
    }
    public String getProfile() {
        return profile;
    }
    public void setProfile(String profile) {
        this.profile = profile;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", version=" + version +
                ", name='" + name + '\'' +
                ", moblie='" + moblie + '\'' +
                ", aaVui='" + aaVui + '\'' +
                ", profile='" + profile + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}