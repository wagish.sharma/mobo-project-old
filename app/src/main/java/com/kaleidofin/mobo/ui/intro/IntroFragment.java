package com.kaleidofin.mobo.ui.intro;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.utils.MenuUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroFragment extends Fragment {
    @BindView(R.id.btn_continue_login)
    Button login;

    private static final String TAG = "IntroFragment";
    private FirebaseAuth mAuth;
    private NavController navController;

    public IntroFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_intro, container, false);
        ButterKnife.bind(this, view);

        mAuth = FirebaseAuth.getInstance();

        final FirebaseUser currentUser = mAuth.getCurrentUser();
        // configure navController
        navController = NavHostFragment.findNavController(this);
        Log.d(TAG, "onCreateView: user auhenticated? :"+(currentUser != null));
        // hide navigation menu
        MenuUtils.hideNavigationMenu(getActivity());
        //mAuth.signOut();
        Log.d(TAG, "onCreateView: singout");
        if(currentUser!=null){
            Log.d(TAG, "onCreateView: singout didnt work");
            login.setOnClickListener(v -> navController.navigate(R.id.action_introFragment_to_registerFragment));//(R.id.action_introFragment_to_registerFragment));
            return  view;
        }else{

            Log.d(TAG, "onCreateView: singout work");
            // hide navigation menu
            MenuUtils.hideNavigationMenu(getActivity());

            // actions
            login.setOnClickListener(v -> navController.navigate(R.id.action_introFragment_to_registerFragment));//action_introFragment_to_dashboardFragment

            return view;
       }



    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated: ");
        super.onViewCreated(view, savedInstanceState);
        fragmentNavigation(view);
    }

    private void fragmentNavigation(View view) {
        mAuth = FirebaseAuth.getInstance();

        final FirebaseUser currentUser = mAuth.getCurrentUser();
        // configure navController
        navController = NavHostFragment.findNavController(this);
        Log.d(TAG, "onCreateView: user auhenticated? :"+(currentUser != null));
        // hide navigation menu

        if(currentUser!=null){
            //check for user state
            //then navigate accordingly
            navController.navigate(R.id.action_introFragment_to_chatIntroFragment);//(R.id.action_introFragment_to_registerFragment));

        }else{


            // hide navigation menu
            // MenuUtils.hideNavigationMenu(getActivity());

            // actions
            // navController.navigate(R.id.action_introFragment_to_registerFragment);//action_introFragment_to_dashboardFragment

        }

    }
}
