package com.kaleidofin.mobo.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.databinding.SpendingPatternCategoryListItemBinding;
import com.kaleidofin.mobo.model.dto.SpendingPattern;
import java.util.List;
import java.util.Random;

public class SpendingPatternCategoryAdapter extends RecyclerView.Adapter<SpendingPatternCategoryAdapter.SpendingPatternCategoryViewHolder> {

  private List<SpendingPattern> spendingPatternCatagories;
  private int[] colorArray;
  private Random random;
  public SpendingPatternCategoryAdapter(int[] colorArray) {
    this.colorArray = colorArray;
    random = new Random();
  }

  public void setSpendingPatternCatagories(
      List<SpendingPattern> spendingPatternCatagories) {
    this.spendingPatternCatagories = spendingPatternCatagories;
    notifyDataSetChanged();
  }

  @NonNull
  @Override
  public SpendingPatternCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
      int viewType) {
    SpendingPatternCategoryListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
        R.layout.spending_pattern_category_list_item,parent,false);
    return new SpendingPatternCategoryViewHolder(binding,spendingPatternCatagories);
  }

  @Override
  public void onBindViewHolder(@NonNull SpendingPatternCategoryViewHolder holder, int position) {
    if(position != RecyclerView.NO_POSITION){
      holder.bind(spendingPatternCatagories.get(position),colorArray[random.nextInt(colorArray.length)]);
    }
  }

  @Override
  public int getItemCount() {
    return spendingPatternCatagories == null? 0 : spendingPatternCatagories.size();
  }

  public static class SpendingPatternCategoryViewHolder extends RecyclerView.ViewHolder{
    private SpendingPatternCategoryListItemBinding binding;
    public SpendingPatternCategoryViewHolder(SpendingPatternCategoryListItemBinding binding, List<SpendingPattern> spendingPatternCatagories) {
      super(binding.getRoot());
      this.binding = binding;
    }

    public void bind(SpendingPattern spendingPattern, @ColorInt int colorId){
      this.binding.setSpendingCategoryAmount(spendingPattern.getAmount());
      this.binding.setSpendingCategoryTitle(spendingPattern.getCategory());
      this.binding.setSpentPercentage(spendingPattern.getSpendPercentile());
      this.binding.setColor(colorId);
    }
  }

}
