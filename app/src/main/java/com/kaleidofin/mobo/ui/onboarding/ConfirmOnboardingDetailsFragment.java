package com.kaleidofin.mobo.ui.onboarding;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.model.dto.UserOnBoardingDetails;
import com.tyagiabhinav.dialogflowchatlibrary.Chatbot;
import com.tyagiabhinav.dialogflowchatlibrary.ChatbotActivity;
import com.tyagiabhinav.dialogflowchatlibrary.ChatbotSettings;
import com.tyagiabhinav.dialogflowchatlibrary.DialogflowCredentials;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmOnboardingDetailsFragment extends Fragment {

    DatabaseReference ref;
    UserOnBoardingDetails userOnBoardingDetails;
    @BindView(R.id.nameValue)
    TextView name;

    @BindView(R.id.ageValue)
    TextView age;

    @BindView(R.id.incomeValue)
    TextView income;

    @BindView(R.id.isMarriedValue)
    TextView isMarried;

    @BindView(R.id.totalChildren)
    TextView totalChildren;

    @BindView(R.id.isHouseRented)
    TextView isHouseRented;

    @BindView(R.id.btn_continue_recommendation)
    Button continueToRecommendation;

    private NavController navController;

    public ConfirmOnboardingDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_confirm_onboarding_details, container, false);
        userOnBoardingDetails=new UserOnBoardingDetails();
        ButterKnife.bind(this,view);

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        startChat();
        firebaseOnBoardingData();
        navController = Navigation.findNavController(view);
        continueToRecommendation.setOnClickListener(v -> navController.navigate(R.id.action_confirmOnboardingDetailsFragment_to_spendingRecommendationFragment));;

    }

    public void firebaseOnBoardingData(){
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        ref = FirebaseDatabase.getInstance().getReference().child("mobo_users").child(uid);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                userOnBoardingDetails = snapshot.getValue(UserOnBoardingDetails.class);
                if(userOnBoardingDetails != null){
                    Log.d(TAG, "onDataChange: "+userOnBoardingDetails.getName());

                    name.setText(userOnBoardingDetails.getName());
                    age.setText(userOnBoardingDetails.getAge());
                    income.setText(userOnBoardingDetails.getIncome());
                    isMarried.setText(userOnBoardingDetails.getIsMarried());
                    totalChildren.setText(userOnBoardingDetails.getNoOfChildren());
                    isHouseRented.setText(userOnBoardingDetails.getIsHouseRented());
                    //TODO make an api call
                    Log.d(TAG, "onDataChange: No of children"+ userOnBoardingDetails.getNoOfChildren());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void startChat(){
        DialogflowCredentials.getInstance().setInputStream(getResources().openRawResource(R.raw.mobo_onboarding));


        ChatbotSettings.getInstance().setChatbot( new Chatbot.ChatbotBuilder()
//                .setDoAutoWelcome(false) // True by Default, False if you do not want the Bot to greet the user Automatically. Dialogflow agent must have a welcome intent to handle this
//                .setChatBotAvatar(getDrawable(R.drawable.avatarBot)) // provide avatar for your bot if default is not required
//                .setChatUserAvatar(getDrawable(R.drawable.avatarUser)) // provide avatar for your the user if default is not required
                .setShowMic(true) // False by Default, True if you want to use Voice input from the user to chat
                .build());

        Bundle bundle = new Bundle();

        // provide a UUID for your session with the Dialogflow agent
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        Log.d(TAG, "startChat: uid"+currentFirebaseUser.getUid() );
        bundle.putString(ChatbotActivity.SESSION_ID, currentFirebaseUser.getUid() );

        Intent intent = new Intent(getActivity(),ChatbotActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.putExtras(bundle);
        startActivity(intent); //startactivitywithresult  overrdie onactivity result
    }
}
