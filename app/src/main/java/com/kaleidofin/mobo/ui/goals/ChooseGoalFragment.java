package com.kaleidofin.mobo.ui.goals;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import com.kaleidofin.mobo.NavGraphDirections;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.base.AutoClearedValue;
import com.kaleidofin.mobo.base.BaseFragment;
import com.kaleidofin.mobo.base.Resource;
import com.kaleidofin.mobo.databinding.FragmentChooseGoalBinding;
import com.kaleidofin.mobo.model.dto.Goal;
import com.kaleidofin.mobo.model.dto.SpendingPatternGroup;
import com.kaleidofin.mobo.ui.adapter.GoalsAdapter;
import com.kaleidofin.mobo.viewmodel.MoboApplicationViewModel;
import java.util.List;
import org.jetbrains.annotations.NotNull;

public class ChooseGoalFragment extends BaseFragment<MoboApplicationViewModel> {
    //position of shimmer layout in viewFlipper
    private static final int LOADING = 0;
    //position of content layout in viewFlipper
    private static final int STATIC_CONTENT = 1;
    //position of content layout in viewFlipper
    private static final int RECYCLER_VIEW_CONTENT = 2;
    private NavController navController;

private MoboApplicationViewModel applicationViewModel;
private final AutoClearedValue<FragmentChooseGoalBinding> bindingAutoClearedValue = new AutoClearedValue<>();
    private final AutoClearedValue<GoalsAdapter> adapterAutoClearedValue = new AutoClearedValue<>();

    public ChooseGoalFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentChooseGoalBinding binding =  DataBindingUtil.inflate(inflater,R.layout.fragment_choose_goal, container, false);
        bindingAutoClearedValue.setValue(this,binding);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
         applicationViewModel.getCustomerGoals();
        GoalsAdapter goalsAdapter = new GoalsAdapter();
        adapterAutoClearedValue.setValue(this,goalsAdapter);
        bindingAutoClearedValue.get().goalsRecyclerView.setAdapter(goalsAdapter);
        applicationViewModel.getCustomerGoalsLiveData().observe(getViewLifecycleOwner(),this::renderViewState);
        bindingAutoClearedValue.get().childEducationCard.setOnClickListener(view1 -> navigateToCreateGoalFragment("Child Education"));
        bindingAutoClearedValue.get().buyingHomeCard.setOnClickListener(view1 -> navigateToCreateGoalFragment("Buying Home"));
        bindingAutoClearedValue.get().ownVehicleCard.setOnClickListener(view1 -> navigateToCreateGoalFragment("Own Vehicle"));
        bindingAutoClearedValue.get().taxSavingsCard.setOnClickListener(view1 -> navigateToCreateGoalFragment("Tax Savings"));
    }

    @Override
    public MoboApplicationViewModel getViewModel() {
        applicationViewModel = new ViewModelProvider(requireActivity()).get(MoboApplicationViewModel.class);
        return applicationViewModel;
    }


    private void navigateToCreateGoalFragment(String goalName){
        navController.navigate(NavGraphDirections.actionGlobalCreateGoalFragment().setGoalName(goalName));
    }


    private void renderViewState(Resource<List<Goal>> resource){
        if(resource != null){
            switch (resource.status){

                case LOADING:
                    bindingAutoClearedValue.get().setDisPlayedChild(LOADING);
                    bindingAutoClearedValue.get().includedLayout.shimmerProgressLayout.startShimmer();
                    break;
                case ERROR:
                    bindingAutoClearedValue.get().includedLayout.shimmerProgressLayout.stopShimmer();
                    bindingAutoClearedValue.get().setDisPlayedChild(STATIC_CONTENT);
                    break;
                case SUCCESS:
                    bindingAutoClearedValue.get().includedLayout.shimmerProgressLayout.stopShimmer();
                    if(resource.viewState == null || resource.viewState.isEmpty()){
                        bindingAutoClearedValue.get().setDisPlayedChild(STATIC_CONTENT);

                    }else {
                        bindingAutoClearedValue.get().setDisPlayedChild(RECYCLER_VIEW_CONTENT);
                        //set adpater
                        adapterAutoClearedValue.get().setCustomerGoals(resource.viewState);
                    }
                    break;
            }

        }
    }

    @Override
    public int getNavHostFragmentId() {
        return R.id.nav_host_fragment;
    }
}
