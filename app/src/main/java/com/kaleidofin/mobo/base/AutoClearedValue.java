package com.kaleidofin.mobo.base;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

/**
 * A value holder that automatically clears the reference if the Fragment's view is destroyed.
 * @param <T>
 */
public class AutoClearedValue<T> implements LifecycleObserver {
    private T value;

    public void setValue(Fragment fragment, T value) {
        fragment.getViewLifecycleOwner().getLifecycle().removeObserver(this);
        this.value = value;
        fragment.getViewLifecycleOwner().getLifecycle().addObserver(this);
    }

    public T get() {
        if (value != null) {
            return value;
        } else {
            throw new IllegalStateException("Trying to call an auto-cleared value outside of the view lifecycle.");
        }

    }
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onFragmentViewDestroyed(){
        value  = null;
    }

}