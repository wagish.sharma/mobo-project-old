package com.kaleidofin.mobo.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

public class ConsentViewModel extends AndroidViewModel {

    private boolean isConsentRequested = false;
    public ConsentViewModel(@NonNull Application application) {
        super(application);
    }

    public boolean getIsConsentRequested() {
        return isConsentRequested;
    }

    public void setIsConsentRequested(boolean isConsentRequested) {
        this.isConsentRequested = isConsentRequested;
    }



}
