package com.kaleidofin.mobo.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.kaleidofin.mobo.base.BaseViewModel;

public class MoboChatViewModel extends BaseViewModel {
    public boolean isOnBoarded() {
        return isOnBoarded;
    }

    public void setOnBoarded(boolean onBoarded) {
        isOnBoarded = onBoarded;
    }

    private boolean isOnBoarded = false;

    public boolean isOnBoardingChatStarted() {
        return isOnBoardingChatStarted;
    }

    public void setOnBoardingChatStarted(boolean onBoardingChatStarted) {
        isOnBoardingChatStarted = onBoardingChatStarted;
    }

    private boolean isOnBoardingChatStarted = false;
    public MoboChatViewModel(@NonNull Application application) {
        super(application);
    }
}
