package com.kaleidofin.mobo.utils;

import android.content.DialogInterface;
import android.os.Parcel;
import android.os.Parcelable;

public class DialogButtonActions implements Parcelable {

    private DialogInterface.OnClickListener positiveAction;
    private DialogInterface.OnClickListener negativeAction;

    public DialogButtonActions() {
    }

    public DialogButtonActions(DialogInterface.OnClickListener positiveAction, DialogInterface.OnClickListener negativeAction) {
        this.positiveAction = positiveAction;
        this.negativeAction = negativeAction;
    }

    protected DialogButtonActions(Parcel in) {
    }

    public static final Creator<DialogButtonActions> CREATOR = new Creator<DialogButtonActions>() {
        @Override
        public DialogButtonActions createFromParcel(Parcel in) {
            return new DialogButtonActions(in);
        }

        @Override
        public DialogButtonActions[] newArray(int size) {
            return new DialogButtonActions[size];
        }
    };

    public DialogInterface.OnClickListener getPositiveAction() {
        return positiveAction;
    }

    public void setPositiveAction(DialogInterface.OnClickListener positiveAction) {
        this.positiveAction = positiveAction;
    }

    public DialogInterface.OnClickListener getNegativeAction() {
        return negativeAction;
    }

    public void setNegativeAction(DialogInterface.OnClickListener negativeAction) {
        this.negativeAction = negativeAction;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}