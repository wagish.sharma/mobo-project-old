package com.kaleidofin.mobo.utils;

import android.widget.ViewFlipper;
import androidx.databinding.BindingAdapter;

public final class ViewBindings {
  @BindingAdapter("displayedChild")
  public static void setDisplayedChild(ViewFlipper viewFlipper, int childPosition){
    if(viewFlipper != null && viewFlipper.getChildCount() > childPosition
        && viewFlipper.getDisplayedChild() != childPosition){
      viewFlipper.setDisplayedChild(childPosition);
    }
  }
}
